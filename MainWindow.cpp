#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QPluginLoader>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    loadPlugins();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::loadPlugins()
{
    pluginsDir = QDir(qApp->applicationDirPath());

#if defined(Q_OS_WIN)
    if (pluginsDir.dirName().toLower() == "debug" || pluginsDir.dirName().toLower() == "release")
        pluginsDir.cdUp();
#elif defined(Q_OS_MAC)
    if (pluginsDir.dirName() == "MacOS") {
        pluginsDir.cdUp();
        pluginsDir.cdUp();
        pluginsDir.cdUp();
    }
#endif
    pluginsDir.cd("plugins");

    int i = 0;

    foreach (QString fileName, pluginsDir.entryList(QDir::Files)) {
        QPluginLoader loader(pluginsDir.absoluteFilePath(fileName));
        QObject *plugin = loader.instance();
        if (plugin) {
            WebsiteInterface *iWebsite = qobject_cast<WebsiteInterface *>(plugin);
            if(iWebsite)
            {
                //TODO Implement refreshing of the plugins
                //IMPL Clear UploaderList and relaunch function
                qDebug() << "PLUGINS: Adding plugin #" << i;
                WebsiteList.append(iWebsite);
                i++;
                pluginFileNames += fileName;
                qDebug() << "PLUGINS: " << fileName << "added as plugin.";
            }
        }
        else
            qDebug() << "PLUGINS: " << fileName << "is not a valid file.";
    }
}
