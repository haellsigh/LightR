#ifndef WEBSITEINTERFACE
#define WEBSITEINTERFACE

#include <QtPlugin>
#include "MangaStructures.h"
#include <QList>

class WebsiteInterface
{
public:
    virtual ~WebsiteInterface() {}

    virtual QList<Manga::baseManga> getAllMangas() const = 0;
    virtual Manga::Manga getManga(QString name) const = 0;
signals:
};

Q_DECLARE_INTERFACE(WebsiteInterface, "org.LightR.WebsiteInterface/1.0")

#endif // WEBSITEINTERFACE

