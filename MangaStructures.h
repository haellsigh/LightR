#ifndef MANGASTRUCTURES
#define MANGASTRUCTURES

#include <QMap>
#include <QUrl>
#include <QString>
#include <QPixmap>

namespace Manga {

/*!
 * \brief Enum to store information about current status
 * \param Ongoing, Completed, Not yet released
 */
enum Status
{
    Ongoing, Completed, Not_yet_released
};
typedef enum Status Status;

/*!
 * \brief Struct containing base Manga info which you can use to get a Manga struct;
 */
struct baseManga
{
    QString title;
    QUrl mangaUrl;
};
typedef struct baseManga baseManga;

struct Chapter;

/*!
 * \brief Struct containing all the Manga stuff
 */
struct Manga
{
    QString name;
    QStringList alternative_names;
    QStringList genres;
    QStringList authors;
    QStringList artists;
    int rank;
    QString summary;

    QPixmap cover;

    //QMap<int, QMap<QString, Chapter> > chapters;
    QList<Chapter> chapters; // Chapter object?
    Manga() : rank(-1)
    {
    }
    /*Manga() : name(QString()), rank(-1), summary(QString()), cover(QPixmap())
    {
    }*/
};
typedef struct Manga Manga;

struct Chapter
{
    //ex: http://www.mangahere.co/manga/girl_the_wild_s/c039/
    int totalPages;
    // pageLinks[0] will the link to be page 0
    QList<QUrl> imageLinks;
    QList<QPixmap *> pages;
    QUrl link;
};
typedef struct Chapter Chapter;

struct Progress
{
    Manga *manga;
    //manga.chapters[chapter] will point to the chapter current read
    int chapter;
    int page;
};
typedef struct Progress Progress;

} // namespace Manga

#endif // MANGASTRUCTURES
