#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDir>

#include <WebsiteInterface.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    QDir pluginsDir;
    QStringList pluginFileNames;
    QList<WebsiteInterface*> WebsiteList;

    void loadPlugins();
};

#endif // MAINWINDOW_H
