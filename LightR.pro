#-------------------------------------------------
#
# Project created by QtCreator 2015-03-29T17:29:07
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET =    LightR
TEMPLATE =  app

LIBS =      -Lplugins

SOURCES +=  main.cpp\
            MainWindow.cpp

HEADERS +=  MainWindow.h \
    WebsiteInterface.h \
    MangaStructures.h

FORMS +=    MainWindow.ui

include(QDownloadManager/QDownloadManager.pri)
